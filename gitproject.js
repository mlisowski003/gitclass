// plik przedstawia podstawowe możliwości związane z obiektami

(function () {
    var person = {
        name: 'Krzysiek',
        surname: 'Kalinowski',
        born: new Date('1985-12-01'),
        height: 183,
        male: true,
        getFullName: function () {
            return this.name + ' ' + this.surname;
        },
        /**
         * zwraca płeć jako polskie słowo
         * @returns {String}
         */
        getGenderString: function () {
            return this.male ? 'Mężczyzna' : 'Kobieta';

//            // dłuższy zapis:
//            if (this.male) {
//                return 'Mężczyzna';
//            }
//
//            return 'Kobieta';

        }
    };

    /**
     * zwraca wiek, bazując na dacie urodzenia
     * @returns {Number}
     */
    person.getAge = function () {
        var today = new Date(); // dzisiejsza data
        return today.getFullYear() - this.born.getFullYear();
    };

    console.log(person.name); // imię

    var fieldName = 'name';
    console.log(person[fieldName]); // alternatywna metoda dostania się do pola obiektu

    console.log(person.getFullName()); // wypisuje imię i nazwisko
    console.log(person.getAge()); // wiek

    var emptyObject = {}; // deklaracja pustego obiektu
    var emptyObject2 = new Object(); // alternatywny sposób deklaracji

    emptyObject.myValue = 5; // dodaje kolejne pole do obiektu
    console.log(emptyObject.myValue); // wypisze 5
    console.log(emptyObject); // wypisze na konsolę cały obiekt

    emptyObject2.value = 6;

    console.log(person); // wypisze wszystkie pola i metody

    console.log(person.getGenderString()); // wypisze Mężczyzna

    /********* DZIEDZICZENIE ***********/

    // konstruktor klasy Rectangle
    function Rectangle(a, b) {
        this.a = a;
        this.b = b;

        this.someField = 'Jakaś wartość';
        this.method = function(){
            console.log('Robię cokolwiek');
        };

        this.area = function() {
            return this.a * this.b;
        };
    }

    // konstruktor klasy Square
    function Square(a) {
        Rectangle.call(this, a, a); // wywołanie konstruktora klasy nadrzędnej

        this.method = function(){
            console.log('Robię coś innego');
        };
    }

    // podmina prototypu klasy Square
    Square.prototype = Object.create(Rectangle.prototype);
    Square.prototype.constructor = Square;

    var square = new Square(5);
    var rectangle = new Rectangle(5, 6);

    console.log('Czy square jest klasy Rectangle?', square instanceof Square);// true
    console.log('Czy square jest klasy Shape?', square instanceof Rectangle);// true

    Rectangle.prototype.description = 'Jakaś figura';
    // dopisuję pole do wszystkich obiektów klasy Rectangle

    Square.prototype.whichFigure = 'kwadrat';

    console.log(square.description);
    // również w klasie Square dodało się to pole

    console.log(square.whichFigure); // wypisze "kwadrat"
    console.log(rectangle.whichFigure); // undefined - pole nie istnieje

    square.method();
    rectangle.method();

    console.log(square.area());
    console.log(rectangle.area());
})();